package elTanoConstrucciones;

public class GasistaInstalacion extends Servicio {
  private int cantidadArtefactos;
  private double costoIntalacion;


  public GasistaInstalacion(int cantidadArtefactos, double costoInstalacion, Especialista especialista,
                            Cliente clientes, String domicilioCliente, int codigo) {
    super(especialista, clientes, domicilioCliente, codigo, "GasistaInstalacion");

    this.cantidadArtefactos = cantidadArtefactos;
    this.costoIntalacion = costoInstalacion;

  }

  public GasistaInstalacion() {
    super();
  }

  @Override
  public double facturaServicio() {
    return cantidadArtefactos * costoIntalacion;
  }

  @Override
  public String toString() {
    StringBuilder st = new StringBuilder();
    return st.append("GasistaInstalacion [cantidadArtefactos=")
        .append(cantidadArtefactos).append(", costoIntalacion=")
        .append(costoIntalacion).append("]").toString();
  }

}
