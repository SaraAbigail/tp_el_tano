package elTanoConstrucciones;

import java.util.*;

public class EmpresaElTano {
  private static Map<Integer, Servicio> servicios;
  private static Map<Integer, Cliente> clientes;
  private static Map<Integer, Especialista> especialistas;
  private static Map<Integer, Servicio> serviciosFinalizados;
  private static Acumulador acumulador; 

  public EmpresaElTano() {
    this.servicios = new HashMap<>();
    this.clientes = new HashMap<>();
    this.especialistas = new HashMap<>();
    this.serviciosFinalizados = new HashMap<>();
    this.acumulador = new Acumulador();
  }

  public EmpresaElTano(Map<Integer, Servicio> servicios, Map<Integer, Cliente> clientes,
                       Map<Integer, Especialista> especialistas, Map<Integer, Servicio> serviciosFinalizados) {
    this.servicios = servicios;
    this.clientes = clientes;
    this.especialistas = especialistas;
    this.serviciosFinalizados = serviciosFinalizados;
    this.acumulador = new Acumulador();
  }

  /*genera un codigo para un servicio*/
  public static int obtenerCodigoServicio() {
    return servicios.size();
  }


  public static Especialista obtenerEspecialista(int nroEspecialista) {
    for (Integer clave : especialistas.keySet()) {
      Especialista e = especialistas.get(clave);
      if (e.getNroEspecialista() == nroEspecialista) {
        return e;
      }
    }
    return null;
  }

  public static Cliente obtenerCliente(int dni) {
    for (Integer clave : clientes.keySet()) {
      Cliente c = clientes.get(clave);
      if (c.getDni() == dni) {
        return c;
      }
    }
    return null;
  }

  public static void registrarCliente(int dni, String nombre, String telefono) {
    Cliente cliente = new Cliente(dni, nombre, telefono);
    for (Integer clave : clientes.keySet()) {
      Cliente c = clientes.get(clave);
      if (c.getDni() == cliente.getDni()) {
        throw new RuntimeException("el cliente ya se encuentra registrado");
      }
    }
    clientes.put(dni, cliente);
  }

  public static void registrarEspecialista(int nroEspecialista, String nombre,
                                    String telefono, String especialidad) {
    Especialista especialista = new Especialista(nroEspecialista, nombre, telefono, especialidad);

    for (Integer clave : especialistas.keySet()) {
      Especialista e = especialistas.get(clave);
      if (e.getNroEspecialista() == especialista.getNroEspecialista())
        throw new RuntimeException("Especialista ya se encuentra registrado");
    }
    if (!verificarServicioEmpresa(especialista.getServicio())) {
      throw new RuntimeException("servicio no pertenece a la empresa");
    }
    especialistas.put(especialista.getNroEspecialista(),especialista);
  }

  public static int solicitarServicioElectricidad(int dni, int nroEspecialista, String direccion,
                                                  double precioPorHora, int horasTrabajadas) {
    if (obtenerCliente(dni) == null) {
      throw new RuntimeException("El cliente no esta registrado en la empresa");
    }
    if (obtenerEspecialista(nroEspecialista) == null) {
      throw new RuntimeException("El especialista no esta registrado en la empresa");
    }
    if (!realizaServicio("Electricidad", nroEspecialista))
      throw new RuntimeException("El especialista no realiza este servicio");

    if (precioPorHora <= 0 || horasTrabajadas <= 0) {
      throw new RuntimeException("El precio por hora y las horas trabajadas no deben ser menores a 0");
    }
    Servicio servicio = new Electricista(precioPorHora, horasTrabajadas, obtenerEspecialista(nroEspecialista),
        obtenerCliente(dni), direccion, obtenerCodigoServicio());
    servicios.put(servicio.getCodigo(), servicio);

    return servicio.getCodigo();
  }

  public static int solicitarServicioPintura(int dni, int nroEspecialista, String direccion, int metrosCuadrados,
                                             double precioPorMetroCuadrado) {
    if (obtenerCliente(dni) == null)
      throw new RuntimeException("El cliente no esta registrado en la empresa");

    if (obtenerEspecialista(nroEspecialista) == null)
      throw new RuntimeException("El especialista ya esta registrado en la empresa");

    if (!realizaServicio("Pintura", nroEspecialista))
      throw new RuntimeException("El especialista no realiza este servicio");

    if (metrosCuadrados <= 0 || precioPorMetroCuadrado <= 0)
      throw new RuntimeException(
          "El precio por metrosCuadrados y el precioMetroCuadrado no deben ser menores a 0");

    Servicio servicio = new Pintura(metrosCuadrados, precioPorMetroCuadrado, obtenerEspecialista(nroEspecialista),
        obtenerCliente(dni), direccion, obtenerCodigoServicio());
    servicios.put(servicio.getCodigo(), servicio);

    return servicio.getCodigo();
  }

  public static int solicitarServicioPinturaAltura(int dni, int nroEspecialista, String direccion,
                                                   int metrosCuadrados, double precioPorMetroCuadrado,
                                                   int piso, double valorAndamio, double valorSeguro) {
    if (obtenerCliente(dni) == null) {
      throw new RuntimeException("El cliente no esta registrado en la empresa");
    }
    if (obtenerEspecialista(nroEspecialista) == null) {
      throw new RuntimeException("El especialista no esta registrado en la empresa");
    }
    if (!realizaServicio("PinturaEnAltura", nroEspecialista)) {
      throw new RuntimeException("El especialista no realiza este servicio");
    }
    if (metrosCuadrados <= 0 || precioPorMetroCuadrado <= 0) {
      throw new RuntimeException(
          "El precio por metrosCuadrados y el precioMetroCuadrado no deben ser menores a 0");
    }

    Servicio servicio = new PinturaEnAltura(valorSeguro, valorAndamio, piso, metrosCuadrados,
        precioPorMetroCuadrado, obtenerEspecialista(nroEspecialista),
        obtenerCliente(dni), direccion, obtenerCodigoServicio());/*Corregir faltan los parametros del constructor*/
    servicios.put(servicio.getCodigo(), servicio);

    return servicio.getCodigo();
  }

  public static int solicitarServicioGasistaInstalacion(int dni, int nroEspecialista, String direccion,
                                                        int cantidadArtefactos, double costoInstalacion) {
    if (obtenerCliente(dni) == null) {
      throw new RuntimeException("El cliente no esta registrado en la empresa");
    }
    if (obtenerEspecialista(nroEspecialista) == null) {
      throw new RuntimeException("El especialista no esta registrado en la empresa");
    }
    if (!realizaServicio("GasistaInstalacion", nroEspecialista)) {
      throw new RuntimeException("El especialista no realiza este servicio");
    }
    if (cantidadArtefactos <= 0 || costoInstalacion <= 0) {
      throw new RuntimeException(
          "El precio por metrosCuadrados y el precioMetroCuadrado no deben ser menores a 0");
    }

    Servicio servicio = new GasistaInstalacion(cantidadArtefactos, costoInstalacion,
        obtenerEspecialista(nroEspecialista), obtenerCliente(dni), direccion,
        obtenerCodigoServicio());
    servicios.put(servicio.getCodigo(), servicio);

    return servicio.getCodigo();
  }

  public static int solicitarServicioGasistaRevision(int dni, int nroEspecialista, String direccion,
                                                     int cantidadArtefactos, double precioPorArtefacto) {
    if (obtenerCliente(dni) == null) {
      throw new RuntimeException("El cliente no esta registrado en la empresa");
    }
    if (obtenerEspecialista(nroEspecialista) == null) {
      throw new RuntimeException("El especialista no esta registrado en la empresa");
    }
    if (!realizaServicio("GasistaRevision", nroEspecialista)) {
      throw new RuntimeException("El especialista no realiza este servicio");
    }
    if (cantidadArtefactos <= 0 || precioPorArtefacto <= 0) {
      throw new RuntimeException(
          "El precio por la cantidad de artefactos y el costo por instalacion no deben ser menores a 0");
    }
    Servicio servicio = new GasistaRevision(precioPorArtefacto, cantidadArtefactos,
        obtenerEspecialista(nroEspecialista), obtenerCliente(dni), direccion,
        obtenerCodigoServicio());
    servicios.put(servicio.getCodigo(), servicio);
    return servicio.getCodigo();
  }
  
  public static boolean verificarServicioEmpresa(String servicio) {
	    return (servicio.equals("Electricidad") || servicio.equals("GasistaInstalacion")
	        || servicio.equals("GasistaRevision") || servicio.equals("Pintura")
	        || servicio.equals("PinturaEnAltura"));
	  }

  public static double finalizarServicio(int codServicio, double costoMateriales) {

    if (servicios.size() <= codServicio || servicios.get(codServicio) == null) {
      throw new RuntimeException("El codigo de servicio no esta en el sistema");
    }
    Servicio servicio = servicios.get(codServicio);
    serviciosFinalizados.put(servicio.getCodigo(), servicio);
    double precio = servicio.facturaServicio();
    facturacionServicioParticular(precio + costoMateriales, servicio.getNombreServicio());

    return precio + costoMateriales;
  }

  public static void facturacionServicioParticular(double costoTotal, String tipoServicio) {
    switch (tipoServicio) {
      case "GasistaInstalacion":
        acumulador.setGasistaInstalacion(acumulador.getGasistaInstalacion() + costoTotal);
        break;
      case "GasistaRevision":
        acumulador.setGasistaRevision(acumulador.getGasistaRevision() + costoTotal);
        break;
      case "Pintura":
        acumulador.setPintura(acumulador.getPintura() + costoTotal);
        break;
      case "PinturaEnAltura":
        acumulador.setPinturaAltura(acumulador.getPinturaAltura() + costoTotal);
        break;
      case "Electricidad":
        acumulador.setElectricista(acumulador.getElectricista() + costoTotal);
        break;
      default:
        throw new RuntimeException("El tipo de servicio es inválido");
    }

  }

  public void cambiarResponsable(int codServicio, int nroEspecialista) {
    Servicio s = servicios.get(codServicio);
    Especialista e = obtenerEspecialista(nroEspecialista);
    if (e == null || s == null) {
      throw new RuntimeException("El servicio o especialista no esta registrado en el sistema");
    }
    if (!s.getEspecialista().getServicio().equals(e.getServicio())) {
      throw new RuntimeException("El especialista no se especializa en este servicio");
    }

    s.setEspecialista(e);

  }

  public double facturacionTotalPorTipo(String tipoServicio) {
    switch (tipoServicio) {
      case "GasistaIntalacion":
        return acumulador.getGasistaInstalacion();
      case "GasistaRevision":
        return acumulador.getGasistaRevision();
      case "Pintura":
        return acumulador.getPintura();
      case "PinturaEnAltura":
        return acumulador.getPinturaAltura();
      case "Electricidad":
        return acumulador.getElectricista();
      default:
        throw new RuntimeException("El tipo de servicio es inválido");
    }
  }

  public double facturacionTotal() {
    return acumulador.getElectricista() + acumulador.getGasistaInstalacion() + acumulador.getGasistaRevision()
        + acumulador.getPintura() + acumulador.getPinturaAltura();
  }

  static boolean realizaServicio(String servicio, int nroEspecialista) {
    return (obtenerEspecialista(nroEspecialista).getServicio().equals(servicio));
  }

  public static boolean realizoServicio(int nroEspecialista) {
    for (Integer clave : servicios.keySet()) {
      Servicio s = servicios.get(clave);
      if (realizaServicio(s.getNombreServicio(), nroEspecialista)) {
        return true;
      }
    }
    return false;
  }


  public static String listadoServiciosAtendidosPorEspecialista(int nroEspecialista) {
    StringBuilder st = new StringBuilder();
    for (Integer clave : especialistas.keySet()) {
      Especialista e = especialistas.get(clave);
      if (e == null) {
        throw new RuntimeException("El nroEspecialista no está registrado en el sistema");
      }
    }
    if (!realizoServicio(nroEspecialista))
      return "";

    for (Integer clave : servicios.keySet()) {
      Servicio servicio = servicios.get(clave);
      if (servicio.getEspecialista().getNroEspecialista() == nroEspecialista) {
        st.append(" + [ ")
            .append(servicio.getCodigo())
            .append(" - ")
            .append(servicio.getNombreServicio())
            .append(" ] ")
            .append(servicio.getDomicilioCliente())
            .append("\n");
      }
    }
    return st.toString();
  }
 
  public Map<String, Integer> cantidadDeServiciosRealizadosPorTipo() {
	    Map<String,Integer> cantidadServiciosRealizados = new HashMap<>();
	    int cantidadActualGasista = 0;
	    int cantidadActualGasistaIntalacion = 0;
	    int cantidadActualPintura = 0;
	    int cantidadActualPinturaEnAltura = 0;
	    int cantidadActualElectricidad = 0;       
	    for (Servicio s : serviciosFinalizados.values()) {
	        String nombreServicio = s.getNombreServicio();	        
	        switch (nombreServicio) {
	        case "GasistaInstalacion":
	        	cantidadActualGasistaIntalacion++;  	
	        	break;       
	        case "GasistaRevision":
	        	cantidadActualGasista++;      	
	        	break; 
	          case "Pintura":
	        	  cantidadActualPintura++;	        	  
	        	  break;            
	          case "PinturaEnAltura":
	        	  cantidadActualPinturaEnAltura++;
	        	  break; 
	          case "Electricidad":
	        	  cantidadActualElectricidad++;	        	  
	        	  break;           
	        }
	    }
	    cantidadServiciosRealizados.put("GasistaInstalacion", cantidadActualGasistaIntalacion);
	    cantidadServiciosRealizados.put("GasistaRevision", cantidadActualGasista);
	    cantidadServiciosRealizados.put("Pintura", cantidadActualPintura);
	    cantidadServiciosRealizados.put("PinturaEnAltura", cantidadActualPinturaEnAltura);
	    cantidadServiciosRealizados.put("Electricidad", cantidadActualElectricidad);
	    return cantidadServiciosRealizados;
	}


  @Override
  public String toString() {
    StringBuilder st = new StringBuilder();
    st.append("Empresa El Tano").append("\n").append("Especialistas");
    for(Integer e: especialistas.keySet()) {
    	Especialista especialista = especialistas.get(e); 	
    	 if (especialista.getNroEspecialista() !=0) {
    	         st.append(especialista);
    	      }
    }
    st.append("\n").append("Clientes");
    for(Integer c: clientes.keySet()) {
    	Cliente cliente = clientes.get(c);
    	if (cliente.getDni() !=0) {
   	       
    	st.append(clientes.get(c));
    	}
    }
    return st.toString() ;
  }

}