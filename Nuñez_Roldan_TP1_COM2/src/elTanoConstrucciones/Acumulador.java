package elTanoConstrucciones;

public class Acumulador {
  private double gasistaInstalacion;
  private double gasistaRevision;
  private double electricista;
  private double pintura;
  private double pinturaAltura;

  public double getGasistaInstalacion() {
    return gasistaInstalacion;
  }

  public void setGasistaInstalacion(double gasistaInstalacion) {
    this.gasistaInstalacion = gasistaInstalacion;
  }

  public double getGasistaRevision() {
    return gasistaRevision;
  }

  public void setGasistaRevision(double gasistaRevision) {
    this.gasistaRevision = gasistaRevision;
  }

  public double getElectricista() {
    return electricista;
  }

  public void setElectricista(double electricista) {
    this.electricista = electricista;
  }

  public double getPintura() {
    return pintura;
  }

  public void setPintura(double pintura) {
    this.pintura = pintura;
  }

  public double getPinturaAltura() {
    return pinturaAltura;
  }

  public void setPinturaAltura(double pinturaAltura) {
    this.pinturaAltura = pinturaAltura;
  }


}
