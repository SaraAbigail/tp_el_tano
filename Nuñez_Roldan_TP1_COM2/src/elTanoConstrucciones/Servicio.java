package elTanoConstrucciones;

public abstract class Servicio {
  private String nombreServicio;
  private Especialista especialista;
  private String domicilioCliente;
  private int codigo;

  public Servicio() {
  }

  public Servicio(Especialista especialista, Cliente clientes, String domicilioCliente,
                  int codigo, String nombreServicio) {

    this.nombreServicio = nombreServicio;
    this.especialista = especialista;
    this.domicilioCliente = domicilioCliente;
    this.codigo = codigo;
  }

  public abstract double facturaServicio();

  public String getNombreServicio() {
    return nombreServicio;
  }

  public Integer getCodigo() {
    return codigo;
  }

  public String getDomicilioCliente() {
    return domicilioCliente;
  }

  public Especialista getEspecialista() {
    return especialista;
  }

  public void setEspecialista(Especialista especialista) {
    this.especialista = especialista;
  }

  @Override
  public String toString() {
    StringBuilder st = new StringBuilder();
    return st.append("Servicio [nombreServicio=").append(nombreServicio)
        .append(", domicilioCliente=").append(domicilioCliente)
        .append(", codigo=").append(codigo).append("]").toString();
  }


}
