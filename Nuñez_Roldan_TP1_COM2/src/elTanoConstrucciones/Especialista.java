package elTanoConstrucciones;

public class Especialista {
  private int nroEspecialista;
  private String nombre;
  private String telefono;
  private String servicio;

  public Especialista(int nroEspecialista, String nombre, String telefono, String servicio) {

    this.nroEspecialista = nroEspecialista;
    this.nombre = nombre;
    this.telefono = telefono;
    this.servicio = servicio;

  }

  public int getNroEspecialista() {
    return nroEspecialista;
  }

  public String getServicio() {
    return servicio;
  }

  public String getNombre() {
    return nombre;
  }

  public String getTelefono() {
    return telefono;
  }

  public void setNroEspecialista(int nroEspecialista) {
    this.nroEspecialista = nroEspecialista;
  }

  public void setServicio(String servicio) {
    this.servicio = servicio;
  }

  @Override
  public String toString() {
    StringBuilder st = new StringBuilder();
   return  st.append("[nroEspecialista=").append(getNroEspecialista()).append(" nombre=")
        .append(getNombre()).append(" telefono=").append(getTelefono()).append(" servicio=")
        .append(getServicio()).append("]").toString();

  
  }
  
}




