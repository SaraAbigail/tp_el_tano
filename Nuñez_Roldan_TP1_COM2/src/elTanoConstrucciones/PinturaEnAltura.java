package elTanoConstrucciones;

public class PinturaEnAltura extends Pintura {
  private double valorSeguro;
  private double valorAndamio;
  private int piso;

  public PinturaEnAltura(double valorSeguro, double valorAndamio, int piso, int superficie,
                         double precioPorMetroCuadrado, Especialista especialista, Cliente clientes,
                         String domicilioCliente, int codigo) {

    super(superficie, precioPorMetroCuadrado, especialista, clientes, domicilioCliente, codigo);
    this.valorSeguro = valorSeguro;
    this.valorAndamio = valorAndamio;
    this.piso = piso;
  }

  public PinturaEnAltura() {
    super();
  }

  @Override
  public double facturaServicio() {
    double precio = 0;
    if (piso >= 1 || piso <= 5)
        precio = getSuperficie() * getPrecioPorMetroCuadrado() + valorSeguro + valorAndamio;

      
    if (piso >=6 ) {
      precio =( getSuperficie() * getPrecioPorMetroCuadrado() )+ valorSeguro + valorAndamio +(valorAndamio * 0.20);
    }
    
    return precio;
  }

  @Override
  public String toString() {
    StringBuilder st = new StringBuilder();
    return st.append("PinturaEnAltura [valorSeguro=")
        .append(valorSeguro).append(", valorAndamio=")
        .append(valorAndamio)
        .append(", piso=").append(piso).append("]").toString();
  }

}
