package elTanoConstrucciones;

public class Electricista extends Servicio {
  private double valorXhora;
  private int cantHoras;

  public Electricista(double valorXhora, int cantHoras,
                      Especialista especialista, Cliente clientes,
                      String domicilioCliente, int codigo) {
    super(especialista, clientes, domicilioCliente, codigo, "Electricidad");
    this.valorXhora = valorXhora;
    this.cantHoras = cantHoras;
  }

  public Electricista() {
    super();
  }

  @Override
  public double facturaServicio() {
    return valorXhora * cantHoras;
  }

  @Override
  public String toString() {
    StringBuilder st = new StringBuilder();
    return st.append("Electricista [valorXhora=")
        .append(valorXhora).append(", cantHoras=")
        .append(cantHoras).append("]").toString();
  }

}
