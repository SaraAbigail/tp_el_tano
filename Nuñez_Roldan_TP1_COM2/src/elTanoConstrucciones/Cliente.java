package elTanoConstrucciones;

import java.util.Objects;

public class Cliente {
  private int dni;
  private String nombre;
  private String contacto;
  private String domicilio;


  public Cliente(int dni, String nombre, String contacto) {
    this.dni = dni;
    this.nombre = nombre;
    this.contacto = contacto;
  }

  public int getDni() {
    return dni;
  }

  public String getNombre() {
    return nombre;
  }

  public String getContacto() {
    return contacto;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Cliente other = (Cliente) obj;
    return Objects.equals(contacto, other.contacto) && dni == other.dni
        && Objects.equals(domicilio, other.domicilio) && Objects.equals(nombre, other.nombre);
  }

  @Override
  public String toString() {
    StringBuilder st = new StringBuilder();
    return st.append("[dni=").append(getDni()).append(", nombre=").append(getNombre())
        .append(", contacto=").append(getContacto()).append("]").toString();
  }
}
