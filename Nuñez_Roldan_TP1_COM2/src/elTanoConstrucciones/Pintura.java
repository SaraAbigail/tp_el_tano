package elTanoConstrucciones;

public class Pintura extends Servicio {
  private int superficie;
  private double precioPorMetroCuadrado;


  public Pintura() {
    super();
  }

  public Pintura(int superficie, double precioPorMetroCuadrado, Especialista especialista, Cliente clientes,
                 String domicilioCliente, int codigo) {
    super(especialista, clientes, domicilioCliente, codigo, "Pintura");
    this.superficie = superficie;
    this.precioPorMetroCuadrado = precioPorMetroCuadrado;
  }

  public double getSuperficie() {
    return superficie;
  }

  public double getPrecioPorMetroCuadrado() {
    return precioPorMetroCuadrado;
  }

  @Override
  public double facturaServicio() {
    return superficie * precioPorMetroCuadrado;
  }


  @Override
  public String toString() {
    StringBuilder st = new StringBuilder();
    return st.append("Pintura [superficie=")
        .append(superficie).append(", precioPorMetroCuadrado=")
        .append(precioPorMetroCuadrado).append("]").toString();
  }

}
