package elTanoConstrucciones;

public class GasistaRevision extends Servicio {
  private double costoRevision;
  private int cantArtefactos;

  public GasistaRevision(double costoRevision, int cantArtefactos, Especialista especialista, Cliente clientes,
                         String domicilioCliente, int codigo) {
    super(especialista, clientes, domicilioCliente, codigo, "GasistaRevision");
    this.costoRevision = costoRevision;
    this.cantArtefactos = cantArtefactos;
  }

  @Override
  public double facturaServicio() {
    if (cantArtefactos > 10)
      return costoRevision * cantArtefactos - costoRevision * 0.15;//descuento de 15% si es mas de 10 art
    if (cantArtefactos > 5)
      return costoRevision * cantArtefactos - costoRevision * 0.1;//descuento de 15% si es mas de 5 art
    return costoRevision * cantArtefactos;
  }

  @Override
  public String toString() {
    StringBuilder st = new StringBuilder();
    return st.append("GasistaRevision [costoRevision=")
        .append(costoRevision).append(", cantArtefactos=")
        .append(cantArtefactos).append("]").toString();
  }


}
